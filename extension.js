/* extension.js
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.    If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

/* exported init */

'use strict';

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const GObject = imports.gi.GObject;
const St = imports.gi.St;
const Clutter = imports.gi.Clutter;
const Gettext = imports.gettext;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const AggregateMenu = Main.panel.statusArea.aggregateMenu;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;

// Threshold paths
const START_PATH = '/sys/class/power_supply/BAT0/charge_control_start_threshold';
const END_PATH = '/sys/class/power_supply/BAT0/charge_control_end_threshold';
const START_PATH_ALT = '/sys/class/power_supply/BAT0/charge_start_threshold';
const END_PATH_ALT = '/sys/class/power_supply/BAT0/charge_stop_threshold';


const ThinkpadThresholdIndicator = GObject.registerClass({
    GTypeName: 'ThinkpadThresholdIndicator',
}, class ThinkpadThresholdIndicator extends PanelMenu.SystemIndicator {
    _init() {
        super._init();
        this.settings = ExtensionUtils.getSettings();
        
        this._is_available = false;
        this._start_file = null;
        this._end_file = null;
        this._enabled = false;
        
        // Default setings
        this._indicator_mode = 'active';
        this._icons_types = 'symbolic';
        this._threshold_start_value = 75;
        this._threshold_end_value = 80;
        
        this._load_prefs();

        this._is_available = this._available();

        // Indicator
        this._indicator = this._addIndicator();
        this._indicator.gicon = this._unknown_icon;
        this._indicator.visible = true;
        AggregateMenu._indicators.insert_child_at_index(this, 0);
        AggregateMenu._threshold = this;
        
        // Main menu
        this._item = new PopupMenu.PopupSubMenuMenuItem(_('Battery Threshold'), true);
        this._item.icon.gicon = Gio.icon_new_for_string(Me.path + "/threshold-app-symbolic.svg");
        this._item.label.clutter_text.x_expand = true;
        this.menu.addMenuItem(this._item);
        // Current status layout (menu)
        this._item.current_status_box = new St.BoxLayout({
            opacity: 128,
            style_class: 'current-status',
        });
        // Current status icon
        this.current_status_icon = new St.Icon({
            icon_size: 16,
        });
        this.current_status_icon.gicon = this._unknown_icon;
        // Current status label
        this.current_status_label = new St.Label({
            y_align: Clutter.ActorAlign.CENTER,
            text: _('???'),
        });
        this._item.current_status_box.add_child(this.current_status_icon);
        this._item.current_status_box.add_child(this.current_status_label);
        this._item.actor.insert_child_below(this._item.current_status_box, this._item._triangleBin);
        
        // Toggle menu item
        this._toggle = new PopupMenu.PopupMenuItem(_('Not available'));
        this._toggle.sensitive = this._is_available;
        if (this._is_available) {
            this._toggle.connect('activate', () => {
                if (this._is_available) {
                    if (this._enabled) {
                        this._set_threshold(0, 100);
                    } else {
                        this._set_threshold(this._threshold_start_value, this._threshold_end_value);
                    }
                }
            });
        }
        this._item.menu.addMenuItem(this._toggle);

        // Separator
        this._item.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Settings menu item
        this._config = new PopupMenu.PopupMenuItem(_('Settings'));
        this._config.connect('activate', () => {
            ExtensionUtils.openPrefs();
        });
        this._item.menu.addMenuItem(this._config);

        // Search power menu and add below it
        const menuItems = AggregateMenu.menu._getMenuItems();
        const powerMenuIndex = AggregateMenu._power ? menuItems.indexOf(AggregateMenu._power.menu) : -1;
        const menuIndex = powerMenuIndex > -1 ? powerMenuIndex : 4;
        AggregateMenu.menu.addMenuItem(this.menu, menuIndex + 1);
        
        if (this._is_available) {
            this.settings.connect('changed', () => {
                this._load_prefs();
                this._update_gui();
            });
            this._update_gui();
        }
    }
    
    /*
        Load settings
    */
    _load_prefs() {
        this._indicator_mode = this.settings.get_string('show-indicator');
        this._icons_types = this.settings.get_string('icons-types');
        const threshold = this.settings.get_value('threshold').deep_unpack();
        if (this._enabled && (threshold[0] != this._threshold_start_value || threshold[1] != this._threshold_end_value)) {
            this._threshold_start_value = threshold[0];
            this._threshold_end_value = threshold[1];
            this._set_threshold(this._threshold_start_value, this._threshold_end_value)
        }
        this._active_icon = Gio.icon_new_for_string(Me.path + '/threshold-active' + (this._icons_types == 'symbolic' ? '-symbolic' : '') + '.svg');
        this._inactive_icon = Gio.icon_new_for_string(Me.path + '/threshold-inactive' + (this._icons_types == 'symbolic' ? '-symbolic' : '') + '.svg');
        this._unknown_icon = Gio.icon_new_for_string(Me.path + '/threshold-unknown' + (this._icons_types == 'symbolic' ? '-symbolic' : '') + '.svg');
    }
    
    /*
        Set thresholds
    */
    _set_threshold(start, end) {
        // Check integers
        if (!Number.isInteger(start) || !Number.isInteger(end)) {
            logError('Invalid threshold values (Invalid format)! [' + start + '/' + end + ']');
            return;
        }
        // Check that the start is less than the end, that the start is greater than or equal to zero, 
        // and that the end is less than or equal to 100
        if (start >= end || start < 0 || end > 100) {
            logError('Invalid threshold values (Invalid values)! [' + start + '/' + end + ']');
            return;
        }
        // Check that they are multiples of 5
        if ((start % 5) != 0 || (end % 5) != 0) {
            logError('Invalid threshold values (Is not a multiple of 5)! [' + start + '/' + end + ']');
            return;
        }
        // Check ranges
        if (start != 0 || end != 100) {
            if (start < 40 || start > 95 || end < 45 || end > 100) {
                logError('Invalid threshold values (Invalid ranges)! [' + start + '/' + end + ']');
                return;
            }
        }
        
        // Default notification message
        let notify = _('Impossible to apply battery charge thresholds!!!');
        
        // The adjustment process is like this:
        // 1. Reset the start threshold to 0.
        // 2. Reset the end threshold to 100.
        // 3. Apply the new start threshold.
        // 4. Apply the new end threshold.
        //
        // The first two steps are executed to avoid the error (Invalid argument) of any of the 
        // commands when trying to adjust any of the values and causes an invalid configuration condition.
        // Ex:
        // Suppose the current setting is 75/80 and you try to set it to 85/90. 
        // The start threshold is set first by creating, momentarily, an invalid codition (85/80).
        
        if (this._can_write()) {
            // Normal mode, user has permissions
            try {
                this._start_file.replace_contents('0', null, false, Gio.FileCreateFlags.NONE, null);
                this._end_file.replace_contents('100', null, false, Gio.FileCreateFlags.NONE, null);
                
                this._start_file.replace_contents(start.toString(), null, false, Gio.FileCreateFlags.NONE, null);
                this._end_file.replace_contents(end.toString(), null, false, Gio.FileCreateFlags.NONE, null);
                
                if (start != 0 && end != 100) {
                    notify = _('Battery charge thresholds enabled!!!');
                } else {
                    notify = _('Battery charge thresholds disabled!!!');
                }
            } catch (e) {
                logError(e, '_set_threshold');
            }
        } else {
            // Root mode, user does not have permissions
            try {
                const rootCommand = `pkexec sh -c "echo 0 > ${START_PATH}; echo 100 > ${END_PATH}; echo ${start.toString()} > ${START_PATH}; echo ${end.toString()} > ${END_PATH}"`;
                GLib.spawn_command_line_async(rootCommand);
                if (start != 0 && end != 100) {
                    notify = _('Battery charge thresholds enabled!!!');
                } else {
                    notify = _('Battery charge thresholds disabled!!!');
                }
            } catch (e) {
                logError(e, '_set_threshold');
            }
        }
        
        Main.notify(notify);
    }
    
    /*
        Get current thresholds values
    */
    _get_current_values() {
        try {
            const [, start,] = this._start_file.load_contents(null);
            const [, end,] = this._end_file.load_contents(null);
            return [parseInt(start), parseInt(end)];
        } catch (e) {
            logError(e, '_get_current_values');
            return [0, 0];
        } 
    }
    
    /*
        Update GUI components
    */
    _update_gui() {
        const [start, end] = this._get_current_values();
        this._enabled = start != 0 || end != 100;
        this._toggle.label.text = this._enabled ? _('Disable threshold') : _('Enable threshold');
        this._indicator.visible = (this._enabled || this._indicator_mode === 'always') && this._indicator_mode != 'never';
        if (this._enabled) {
            this._indicator.gicon = this._active_icon;
            this.current_status_icon.gicon = this._active_icon;
        } else {
            this._indicator.gicon = this._inactive_icon;
            this.current_status_icon.gicon = this._inactive_icon;
        }
        this.current_status_label.text = _(`${start}/${end} %`);
    }
    
    /*
        Check if thresholds are available
    */
    _available() {
        // Primary paths
        if (GLib.file_test(START_PATH, GLib.FileTest.EXISTS) && GLib.file_test(END_PATH, GLib.FileTest.EXISTS)) {
            // Primary paths
            this._start_file = Gio.File.new_for_path(START_PATH);
            this._end_file = Gio.File.new_for_path(END_PATH);
            if (this._can_read) {
                if(this._start_files_monitors()) {
                    return true;
                }
            }
        }
        // Alternatives paths
        if (GLib.file_test(START_PATH_ALT, GLib.FileTest.EXISTS) && GLib.file_test(END_PATH_ALT, GLib.FileTest.EXISTS)) {
            // Alternaives paths
            this._start_file = Gio.File.new_for_path(START_PATH_ALT);
            this._end_file = Gio.File.new_for_path(END_PATH_ALT);
            if (this._can_read) {
                if(this._start_files_monitors()) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
        Create files monitors
    */
    _start_files_monitors() {
        // Threshold change monitor
        try {
            this._start_monitor = this._start_file.monitor_file(Gio.FileMonitorFlags.NONE, null);
            this._start_monitor.connect("changed", this._update_gui.bind(this));
            this._end_monitor = this._end_file.monitor_file(Gio.FileMonitorFlags.NONE, null);
            this._end_monitor.connect("changed", this._update_gui.bind(this));
            return true;
        } catch (e) {
            logError(e, '_start_files_monitors')
            return false;
        }
    }
    
    /*
        Check if the user can read the thresholds
    */
    _can_read() {
        const startInfo = this._start_file.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
        const endInfo = this._end_file.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
        const canReadStart = startInfo.get_attribute_boolean('access::can-read');
        const canReadEnd = endInfo.get_attribute_boolean('access::can-read');
        return canReadStart && canReadEnd;
    }
    
    /*
        Check if the user can write the thresholds
    */
    _can_write() {
        const startInfo = this._start_file.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
        const endInfo = this._end_file.query_info('access::*', Gio.FileQueryInfoFlags.NONE, null);
        const canWriteStart = startInfo.get_attribute_boolean('access::can-write');
        const canWriteEnd = endInfo.get_attribute_boolean('access::can-write');
        return canWriteStart && canWriteEnd;
    }
    
    destroy() {
        if (this._start_monitor) {
            this._start_monitor.cancel();
            this._start_monitor = null;
        }
        if (this._end_monitor) {
            this._end_monitor.cancel();
            this._end_monitor = null;
        }
        this._item.destroy();
        this.menu.destroy();
        delete AggregateMenu._threshold;
        delete this._start_file;
        delete this._end_file;
        delete this._active_icon;
        delete this._inactive_icon;
        delete this._unknown_icon;
        super.destroy();
    }
});

var thresholdIndicator = null;

function init() {
    ExtensionUtils.initTranslations();
}

function enable() {
    thresholdIndicator = new ThinkpadThresholdIndicator();
}

function disable() {
    thresholdIndicator.destroy();
    thresholdIndicator = null;
}
