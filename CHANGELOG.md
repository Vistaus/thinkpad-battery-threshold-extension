# Changelog

## [Unreleased]
- Sorry for my English, I use a translator. :)

## [v1] - 2022-02-03
### Added
- Initial release.

## [v2] - 2022-02-04
### Fixed
- Change GLib.idle_add() to 'realize' signal in buildPrefsWidget (prefs.js).
- Add settings-schema and gettext-domain in metadata.json.
- Remove unnecessary imports.
- Fix indentations.
- Elimination of unnecessary logs.
- Thanks JustPerfection!!!

## [v3] - 2022-02-05
### Added
- Add alernatives thresholds paths.
- Add available flag and the _init code is restructured according to this addition... Now the submenu is shown even if the thresholds are not available.
### Fixed
- Fix _available function. The test whether the files exist points to the start file twice!

## [v4] - 2022-02-21
### Added
- Add icon type option (symbolic/color).

## [v5] - 2022-02-21
### Fixed
- Fix icon type on start.
