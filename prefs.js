'use strict';

const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GObject = imports.gi.GObject;
const Gettext = imports.gettext;

const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

const Domain = Gettext.domain(Me.metadata.uuid);
const _ = Domain.gettext;
const ngettext = Domain.ngettext;


const PrefsWidget = GObject.registerClass(
    class PrefsWidget extends Gtk.Grid {
        _init (params) {
            super._init(params);
            
            this.margin_start = 20;
            this.margin_end = 20;
            this.column_spacing = 12;
            this.row_spaciong = 12;
            
            this.settings = ExtensionUtils.getSettings();
            // Settings are applied on dialog close to avoid many threshold changes
            this.settings.delay();
            
            // Initial values
            let auto_start = this.settings.get_boolean('auto-start');
            const threshold = this.settings.get_value('threshold').deep_unpack();
            let start_value = threshold[0];
            let end_value = threshold[1];
            
            // Check threshold values or set default
            if (start_value < 40 || start_value > 95 || start_value % 5 != 0) {
                start_value = 75;
            }
            
            if (end_value < 45 || end_value > 100 || end_value % 5 != 0) {
                end_value = 80;
            }
            
            if (start_value >= end_value) {
                start_value = 75;
                end_value = 80;
            }
            if (auto_start) {
                start_value = end_value - 5;
            }
            
            // Title
            let title = new Gtk.Label({
                label: `<b>${_('Battery charge threshold')}</b>`,
                halign: Gtk.Align.CENTER,
                margin_top: 20,
                use_markup: true,
            });
            this.attach(title, 0, 0, 2, 1);
            
            // Info    
            let infoLabel = new Gtk.Label({
                label: _('If you mainly use the system with the AC power adapter connected and only use the battery sporadically, you can increase battery life by setting the maximum charge value to less than 100%. This is useful because batteries that are used sporadically have a longer lifespan when kept at less than full charge.'),
                halign: Gtk.Align.START,
                wrap: true,
                use_markup: true,
                margin_top: 20,
            });
            this.attach(infoLabel, 0, 1, 2, 1);
            
            // Show Indicator
            let showIndicatorLabel = new Gtk.Label({
                margin_top: 20,
                label: _('Show indicator'),
                halign: Gtk.Align.START,
            });
            
            let showIndicatorMenu = new Gtk.ComboBoxText({
                margin_top: 20,
            });
            showIndicatorMenu.append('never', _('Never'));
            showIndicatorMenu.append('active', _('On active'));
            showIndicatorMenu.append('always', _('Always'));
            this.attach(showIndicatorLabel, 0, 2, 1, 1);
            this.attach(showIndicatorMenu, 1, 2, 1, 1);
            
            this.settings.bind('show-indicator', showIndicatorMenu, 'active-id', Gio.SettingsBindFlags.DEFAULT);

            // Icon type
            let iconTypeLabel = new Gtk.Label({
                margin_top: 20,
                label: _('Icon type'),
                halign: Gtk.Align.START,
            });

            let iconTypeMenu = new Gtk.ComboBoxText({
                margin_top: 20,
            });
            iconTypeMenu.append('symbolic', _('Symbolic'));
            iconTypeMenu.append('color', _('Color'));
            this.attach(iconTypeLabel, 0, 3, 1, 1);
            this.attach(iconTypeMenu, 1, 3, 1, 1);

            this.settings.bind('icons-types', iconTypeMenu, 'active-id', Gio.SettingsBindFlags.DEFAULT);
            
            // Start value
            let startValueLabel = new Gtk.Label({
                margin_top: 20,
                label: _('Start charging value'),
                halign: Gtk.Align.START,
            });
            
            this.startValueMenu = new Gtk.ComboBoxText({
                 margin_top: 20,
                sensitive: !auto_start,
            });
            this.startValueMenu.append('40', _('40 %'));
            this.startValueMenu.append('45', _('45 %'));
            this.startValueMenu.append('50', _('50 %'));
            this.startValueMenu.append('55', _('55 %'));
            this.startValueMenu.append('60', _('60 %'));
            this.startValueMenu.append('65', _('65 %'));
            this.startValueMenu.append('70', _('70 %'));
            this.startValueMenu.append('75', _('75 %'));
            this.startValueMenu.append('80', _('80 %'));
            this.startValueMenu.append('85', _('85 %'));
            this.startValueMenu.append('90', _('90 %'));
            this.startValueMenu.append('95', _('95 %'));
            this.startValueMenu.set_active_id(start_value.toString());
            this.attach(startValueLabel, 0, 4, 1, 1);
            this.attach(this.startValueMenu, 1, 4, 1, 1);
            
            this.startValueMenu.connect('changed', () => {
                start_value = Number.parseInt(this.startValueMenu.get_active_id());
                if (start_value >= end_value) {
                    end_value = start_value + 5;
                    this.endValueMenu.set_active_id(end_value.toString());
                }
                this.settings.set_value('threshold', new GLib.Variant(
                    '(ii)', [start_value, end_value])
                );
            });
            
            // End value
            let endValueLabel = new Gtk.Label({
                margin_top: 20,
                label: _('Stop charging value'),
                halign: Gtk.Align.START,
            });
            
            this.endValueMenu = new Gtk.ComboBoxText({
                margin_top: 20,
            });
            this.endValueMenu.append('45', _('45 %'));
            this.endValueMenu.append('50', _('50 %'));
            this.endValueMenu.append('55', _('55 %'));
            this.endValueMenu.append('60', _('60 %'));
            this.endValueMenu.append('65', _('65 %'));
            this.endValueMenu.append('70', _('70 %'));
            this.endValueMenu.append('75', _('75 %'));
            this.endValueMenu.append('80', _('80 %'));
            this.endValueMenu.append('85', _('85 %'));
            this.endValueMenu.append('90', _('90 %'));
            this.endValueMenu.append('95', _('95 %'));
            this.endValueMenu.append('100', _('100 %'));
            this.endValueMenu.set_active_id(end_value.toString());
            this.attach(endValueLabel, 0, 5, 1, 1);
            this.attach(this.endValueMenu, 1, 5, 1, 1);
            
            this.endValueMenu.connect('changed', () => {
                end_value = Number.parseInt(this.endValueMenu.get_active_id());
                if (end_value <= start_value || auto_start) {
                    start_value = end_value - 5;
                    this.startValueMenu.set_active_id(start_value.toString());
                }
                this.settings.set_value('threshold', new GLib.Variant(
                    '(ii)', [start_value, end_value])
                );
            });
            
            // Auto start value
            let autoStartLabel = new Gtk.Label({
                margin_top: 20,
                label: _('Automatically set charge start threshold when battery is 5% below charge stop threshold'),
                halign: Gtk.Align.START,
            });
            let autoStartSwitch = new Gtk.Switch({
                margin_top: 20,
                active: auto_start,
                halign: Gtk.Align.END,
            });
            this.attach(autoStartLabel, 0, 6, 1, 1);
            this.attach(autoStartSwitch, 1, 6, 1, 1);
            
            autoStartSwitch.connect('state-set', () => {
                auto_start = autoStartSwitch.get_active();
                this.settings.set_boolean('auto-start', auto_start);
                this.startValueMenu.sensitive = !autoStartSwitch.get_active();
                start_value = end_value - 5;
                this.startValueMenu.set_active_id(start_value.toString());
            });
            
            // Save info
            let saveInfoLabel = new Gtk.Label({
                label: '<b>' + _('Information: Changes will be applied when closing this dialog') + '</b>',
                halign: Gtk.Align.CENTER,
                wrap: true,
                margin_top: 20,
                margin_bottom: 20,
                use_markup: true,
            });
            this.attach(saveInfoLabel, 0, 7, 2, 1);
        }
        
        saveSettings() {
            this.settings.apply();
        }
    }
);

function init() {
    ExtensionUtils.initTranslations();
}

function buildPrefsWidget() {
    let prefsWidget = new PrefsWidget();
    prefsWidget.connect('realize', () => {
        let window = prefsWidget.get_root();
        window.resizable = false;
        window.connect('close-request', () => {
            prefsWidget.saveSettings();
        });
    });
    return prefsWidget;
}
