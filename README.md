# Thinkpad Battery Threshold Extension

Gnome extension for Enable/Disable battery threshold on Lenovo Thinkpad laptops


Enable/Disable battery threshold on Lenovo Thinkpad laptops (If available)

To check if the function is available, see if the files <b>charge_control_start_threshold</b> and <b>charge_control_end_threshold</b> (valid alternatives: <b>charge_start_threshold</b> and <b>charge_stop_threshold</b>) exist in the <b>/sys/class/power_supply/BAT0</b> directory, also you must have permissions to read and write (in case you do not have write permissions, the root password will be requested to modify the values).
